<?php

class Post extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'posts';

    /**
     * The primary key of the table.
     * @var string
     */
    protected $primaryKey = 'post_id';

    /**
     * The fillable attributes of the table.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'post_type',
        'post_title',
        'post_content',
        'vote_count',
        'edited_at',
    ];

    /**
     * Relationship with the User model.
     * @return mixed
     */
    public function user() {
        return $this->hasOne('User', 'user_id', 'user_id');
    }

    /**
     * Relationship with the Comment model.
     * @return mixed
     */
    public function comments() {
        return (
            $this->hasMany('Comment', 'parent_id', 'post_id')
                ->where('parent_type', '=', 'POST')
        );
    }

    /**
     * Counts all comments related to a post.
     * @return int|mixed
     */
    public function countComments() {
        return Comment::countChildren($this->comments()->get());
    }

    /**
     * Searches the model for the query and returns the results.
     * @param $query
     * @return mixed
     */
    public static function search($query) {
        $post_titles = Post::where('post_title', 'LIKE', "%$query%")->get();
        $post_contents = Post::where('post_content', 'LIKE', "%$query%")->get();
        return $post_titles->merge($post_contents);
    }
}
