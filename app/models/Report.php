<?php

class Report extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'reports';

    /**
     * The primary key of the table.
     * @var string
     */
    protected $primaryKey = 'report_id';

    /**
     * The fillable attributes of the table.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
        'item_type',
        'report_reason',
    ];
}
