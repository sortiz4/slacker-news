<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {
    use UserTrait, RemindableTrait;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary key of the table.
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * The fillable attributes of the table.
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
        'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function comments() {
        return $this->hasMany('Comment', 'user_id', 'user_id');
    }

    public function posts() {
        return $this->hasMany('Post', 'user_id', 'user_id');
    }
}
