<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Vote extends Eloquent {
    use SoftDeletingTrait;

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'votes';

    /**
     * The primary key of the table.
     * @var string
     */
    protected $primaryKey = 'vote_id';

    /**
     * The fillable attributes of the table.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'parent_id',
        'parent_type',
    ];

    /**
     * Relationship with the User model.
     * @return mixed
     */
    public function user() {
        return $this->hasOne('User', 'user_id', 'user_id');
    }
}
