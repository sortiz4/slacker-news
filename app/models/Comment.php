<?php

class Comment extends Eloquent {
    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'comments';

    /**
     * The primary key of the table.
     * @var string
     */
    protected $primaryKey = 'comment_id';

    /**
     * The fillable attributes of the table.
     * @var array
     */
    protected $fillable = [
        'user_id',
        'parent_id',
        'parent_type',
        'comment_content',
        'vote_count',
        'edited_at',
    ];

    /**
     * Relationship with the User model.
     * @return mixed
     */
    public function user() {
        return $this->hasOne('User', 'user_id', 'user_id');
    }

    /**
     * Relationship with the Comment model.
     * @return mixed
     */
    public function comments() {
        return (
            $this->hasMany('Comment', 'parent_id', 'comment_id')
                ->where('parent_type', '=', 'COMMENT')
        );
    }

    /**
     * Recursively counts all children related to a comment.
     * @param $comments
     * @return int|mixed
     */
    public static function countChildren($comments) {
        $sum = 0;
        foreach ($comments as $comment) {
            // Get a list of all child comments
            $children = $comment->comments()->get();

            // Count the children as well
            if ($children->count() > 0) {
                $sum += Comment::countChildren($children);
            }
        }
        return $sum + $comments->count();
    }

    /**
     * Searches the model for the query and returns the results.
     * @param $query
     * @return mixed
     */
    public static function search($query) {
        return Comment::where('comment_content', 'LIKE', "%$query%")->get();
    }
}
