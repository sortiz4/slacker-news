<?php

class AuthenticationController extends BaseController {
    /**
     * A list of possible error messages.
     * @var array
     */
    private $errorMessage = [
        'Invalid username or password.',
    ];

    /**
     * A list of possible success messages.
     * @var array
     */
    private $successMessage = [
        'You have logged in.',
        'You have logged out.',
    ];

    /**
     * Shows the log in view if the user is not logged in. Otherwise they are
     * redirected to the feed.
     * @return mixed
     */
    public function showAuthentication() {
        if (Auth::check()) {
            return Redirect::route('feed');
        } else {
            return View::make('login');
        }
    }

    /**
     * Logs the user into the web service and redirects them back to the feed.
     * @return mixed
     */
    public function authentication() {
        $validation = Validator::make(
            Input::all(),
            [
                'username' => 'required|alpha_dash|max:15',
                'password' => 'required|min:5|max:60',
            ]
        );
        if ($validation->fails()) {
            Session::flash('errorMessage', $validation->errors()->first());
            return Redirect::back()->withInput();
        }
        if (Input::has('remember')) {
            $remember = true;
        } else {
            $remember = false;
        }
        if (Auth::attempt(Input::only('username', 'password'), $remember)) {
            Session::flash('successMessage', $this->successMessage[0]);
            return Redirect::route('feed');
        } else {
            Session::flash('errorMessage', $this->errorMessage[0]);
            return Redirect::back()->withInput();
        }
    }

    /**
     * Fetch Google account credentials for logging in.
     * @return mixed
     */
    public function googleAuthentication() {
        // Get the authorization code and service provider
        $code = Input::get('code');
        $googleService = OAuth::consumer('Google');

        // Authenticate if the authorization code was provided
        if (!empty($code)) {
            // Request the user's information
            $url = 'https://www.googleapis.com/oauth2/v1/userinfo';
            $result = json_decode($googleService->request($url), true);

            // Construct a valid username from the email address
            $email = explode('@', $result['email']);
            $username = preg_replace('/[^0-9A-Za-z_-]+/', '_', $email[0]);

            // Return to the sign up page with the username
            Input::replace(['username' => $username]);
            return Redirect::route('login')->withInput();
        } else {
            // Redirect to the Google authorization page
            $url = $googleService->getAuthorizationUri();
            return Redirect::to((string)$url);
        }
    }

    /**
     * Logs the user out of the web service if they are logged in and redirects
     * them to the feed.
     * @return mixed
     */
    public function deauthentication() {
        if (Auth::check()) {
            Auth::logout();
            Session::flush();
            Session::flash('successMessage', $this->successMessage[1]);
        }
        return Redirect::route('feed');
    }
}
