<?php

class UserController extends BaseController {
    /**
     * Shows the user profile (currently empty).
     * @param $username
     * @return mixed
     */
    public function showUser($username) {
        if (User::where('username', '=', $username)->exists()) {
            // Display the user if the user exists
            $user = User::where('username', '=', $username)->first();
            return View::make('user', ['user' => $user]);
        } else {
            return App::abort(404);
        }
    }

    /**
     * Shows a list of posts made by the user.
     * @param $username
     * @return mixed
     */
    public function showUserPosts($username) {
        if (User::where('username', '=', $username)->exists()) {
            // Show the user and their posts if the user exists
            $user = User::where('username', '=', $username)->first();
            $posts = Post::where('user_id', '=', $user->user_id)->get();
            return View::make(
                'user',
                [
                    'user' => $user,
                    'posts' => $posts,
                ]
            );
        } else {
            return App::abort(404);
        }
    }


    /**
     * Shows a list of comments made by the user.
     * @param $username
     * @return mixed
     */
    public function showUserComments($username) {
        if (User::where('username', '=', $username)->exists()) {
            // Show the user and their comments if the user exists
            $user = User::where('username', '=', $username)->first();
            $comments = Comment::where('user_id', '=', $user->user_id)->get();
            return View::make(
                'user',
                [
                    'user' => $user,
                    'comments' => $comments,
                ]
            );
        } else {
            return App::abort(404);
        }
    }

    /**
     * Shows a list of items voted on by the user.
     * @param $username
     * @return mixed
     */
    public function showUserFavorites($username) {
        if (User::where('username', '=', $username)->exists()) {
            // Show the user and their favorites if the user exists
            $user = User::where('username', '=', $username)->first();
            $votes = Vote::where('user_id', '=', $user->user_id)->get();
            return View::make(
                'user',
                [
                    'user' => $user,
                    'votes' => $votes,
                ]
            );
        } else {
            return App::abort(404);
        }
    }
}
