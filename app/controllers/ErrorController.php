<?php

class ErrorController extends BaseController {
    /**
     * Generates a missing error (404).
     * @return mixed
     */
    public function missing() {
        return App::abort(404);
    }
}
