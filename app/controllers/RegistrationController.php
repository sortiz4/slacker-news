<?php

class RegistrationController extends BaseController {
    /**
     * A list of possible error messages.
     * @var array
     */
    private $errorMessage = [
        'Something went wrong.',
    ];

    /**
     * A list of possible success messages.
     * @var array
     */
    private $successMessage = [
        'You have signed up. Please log in.',
    ];

    /**
     * Shows the sign up view if the user is not logged in. Otherwise they are
     * redirected to the feed.
     * @return mixed
     */
    public function showRegistration() {
        if (Auth::check()) {
            return Redirect::route('feed');
        } else {
            return View::make('signup');
        }
    }

    /**
     * Fetch Google account credentials for signing up.
     * @return mixed
     */
    public function googleRegistration() {
        // Get the authorization code and service provider
        $code = Input::get('code');
        $googleService = OAuth::consumer('Google');

        // Register if the authorization code was provided
        if (!empty($code)) {
            // Request the user's information
            $url = 'https://www.googleapis.com/oauth2/v1/userinfo';
            $result = json_decode($googleService->request($url), true);

            // Construct a valid username from the email address
            $email = explode('@', $result['email']);
            $username = preg_replace('/[^0-9A-Za-z_-]+/', '_', $email[0]);

            // Return to the sign up page with the username
            Input::replace([
                'email' => $result['email'],
                'username' => $username,
            ]);
            return Redirect::route('signup')->withInput();
        } else {
            // Redirect to the Google authorization page
            $url = $googleService->getAuthorizationUri();
            return Redirect::to((string)$url);
        }
    }

    /**
     * Signs the user up and redirects them to the log in view.
     * @return mixed
     */
    public function registration() {
        $validation = Validator::make(
            Input::all(),
            [
                'email' => 'required|email|max:255|unique:users',
                'username' => 'required|alpha_dash|max:15|unique:users',
                'password' => 'required|min:5|max:60|confirmed',
            ]
        );
        if ($validation->fails()) {
            Session::flash('errorMessage', $validation->errors()->first());
            return Redirect::back()->withInput();
        }
        $email = Input::get('email');
        $username = Input::get('username');
        $password = Input::get('password');
        try {
            User::create([
                'email' => $email,
                'username' => $username,
                'password' => Hash::make($password),
            ]);
        } catch (Exception $exc) {
            Log::error($exc);
            Session::flash('errorMessage', $this->errorMessage[0]);
            return Redirect::back()->withInput();
        }
        Session::flash('successMessage', $this->successMessage[0]);
        return Redirect::route('login');
    }
}
