<?php

class HomeController extends BaseController {
    /**
     * The home page will be shown on entry.
     * @return mixed
     */
    public function showHome() {
        $posts = Post::orderBy('created_at', 'desc')->paginate(5);
        return View::make('feed', ['posts' => $posts]);
    }

    /**
     * This is the about page.
     * @return mixed
     */
    public function showAbout() {
        return View::make('about');
    }
}
