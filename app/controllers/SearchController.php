<?php

class SearchController extends BaseController {
    /**
     * Displays the results of a search query if it exists.
     * @return mixed
     */
    public function showSearch() {
        if (Input::has('q')) {
            // Get the query and search the models
            $query = Input::get('q');
            $posts = Post::search($query);
            $comments = Comment::search($query);

            // Return a more complete view
            return View::make(
                'search',
                [
                    'posts' => $posts,
                    'comments' => $comments,
                ]
            );
        } else {
            // Return a basic view otherwise
            return View::make('search');
        }
    }
}
