<?php

class PostController extends BaseController {
    /**
     * A list of possible comment types.
     * @var array
     */
    private $parentType = [
        'COMMENT',
        'POST',
    ];

    /**
     * A list of possible post types.
     * @var array
     */
    private $postType = [
        'LINK',
        'TEXT',
    ];

    /**
     * A list of possible error messages.
     * @var array
     */
    private $errorMessage = [
        'Something went wrong.',
    ];

    /**
     * A list of possible warning messages.
     * @var array
     */
    private $warningMessage = [
        'Please log in before posting.',
        'Please log in before voting.',
    ];

    /**
     * A list of possible success messages.
     * @var array
     */
    private $successMessage = [
        'Your post has been submitted.',
    ];

    /**
     * If the user is logged in, a link submission page will be created.
     * Otherwise, they will be directed to log in.
     * @return mixed
     */
    public function showSubmitLink() {
        if (Auth::check()) {
            return View::make('submit', ['type' => $this->postType[0]]);
        } else {
            Session::flash('warningMessage', $this->warningMessage[0]);
            return Redirect::route('login');
        }
    }

    /**
     * If the user is logged in, a text submission page will be created.
     * Otherwise, they will be directed to log in.
     * @return mixed
     */
    public function showSubmitText() {
        if (Auth::check()) {
            return View::make('submit', ['type' => $this->postType[1]]);
        } else {
            Session::flash('warningMessage', $this->warningMessage[0]);
            return Redirect::route('login');
        }
    }

    /**
     * Shows the given post.
     * @return mixed
     */
    public function showPost($id) {
        // Fetch the post and the immediately related comments
        $post = Post::find($id);
        if (empty($post)) {
            // Throw an error if the post isn't found
            return App::abort(404);
        }
        $comments = (
            Comment::where('parent_id', '=', $id)
                ->where('parent_type', '=', $this->parentType[1])
                ->get()
        );
        return View::make(
            'post',
            [
                'post' => $post,
                'comments' => $comments,
            ]
        );
    }

    /**
     * Creates a new post in the model.
     * @return mixed
     */
    public function post() {
        if (Auth::check()) {
            // Validate the post by type
            $type = Input::get('type');
            if ($type == 'TEXT') {
                // Validate text posts (length validation)
                $validation = Validator::make(
                    Input::all(),
                    [
                        'post_title' => 'required|max:255',
                        'post_content' => 'max:10000',
                    ]
                );
                if ($validation->fails()) {
                    Session::flash('errorMessage', $validation->errors()->first());
                    return Redirect::back()->withInput();
                }
            } else {
                // Validate link posts (URL validation)
                $validation = Validator::make(
                    Input::all(),
                    [
                        'post_title' => 'required|max:255',
                        'post_content' => 'required|url',
                    ]
                );
                if ($validation->fails()) {
                    Session::flash('errorMessage', $validation->errors()->first());
                    return Redirect::back()->withInput();
                }
            }
            $post_title = Input::get('post_title');
            $post_content = Input::get('post_content');
            try {
                // Create the post
                $post = Post::create([
                    'user_id' => Auth::id(),
                    'post_type' => $type,
                    'post_title' => $post_title,
                    'post_content' => $post_content,
                    'vote_count' => 0,
                ]);
            } catch (Exception $exc) {
                Log::error($exc);
                Session::flash('errorMessage', $this->errorMessage[0]);
                return Redirect::back()->withInput();
            }
        } else {
            // Redirect the user to log in otherwise
            Session::flash('warningMessage', $this->warningMessage[0]);
            return Redirect::route('login');
        }
        // Redirect the user to their new post
        Session::flash('successMessage', $this->successMessage[0]);
        return Redirect::route('post', $post->post_id);
    }

    /**
     * Allows a user to vote on a post.
     * @param $post_id
     * @return mixed
     */
    public function vote($post_id) {
        if (Auth::check()) {
            // Declare parent for later use
            $post = NULL;

            // Fetch the first vote record
            $vote = (
                Vote::withTrashed()
                    ->where('user_id', '=', Auth::id())
                    ->where('parent_id', '=', $post_id)
                    ->where('parent_type', '=', 'POST')
                    ->first()
            );
            try {
                if (!empty($vote)) {
                    if (!empty($vote->deleted_at)) {
                        // Restore the soft deleted vote and increment the vote count
                        $vote->restore();
                        DB::transaction(function() use($post_id, &$post) {
                            $post = Post::find($post_id);
                            $post->vote_count += 1;
                            $post->save();
                        });
                    } else {
                        // Soft delete the vote and decrement the vote count
                        $vote->delete();
                        DB::transaction(function() use($post_id, &$post) {
                            $post = Post::find($post_id);
                            $post->vote_count -= 1;
                            $post->save();
                        });
                    }
                } else {
                    // Create a vote record and increment the vote count
                    Vote::create([
                        'user_id' => Auth::id(),
                        'parent_id' => $post_id,
                        'parent_type' => 'POST',
                    ]);
                    DB::transaction(function() use($post_id, &$post) {
                        $post = Post::find($post_id);
                        $post->vote_count += 1;
                        $post->save();
                    });
                }
            } catch (Exception $exc) {
                Log::error($exc);
                Session::flash('errorMessage', $this->errorMessage[0]);
                return Response::json([
                    'success' => false,
                    'location' => URL::route('login'),
                ]);
            }
        } else {
            // Redirect the user to log in otherwise
            Session::flash('warningMessage', $this->warningMessage[1]);
            return Response::json([
                'success' => false,
                'location' => URL::route('login'),
            ]);
        }
        // Update the view with the new vote count
        return Response::json([
            'success' => true,
            'vote_count' => $post->vote_count,
        ]);
    }
}
