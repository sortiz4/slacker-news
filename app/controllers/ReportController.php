<?php

class ReportController extends BaseController {
    /**
     * A list of possible error messages.
     * @var array
     */
    private $errorMessage = [
        'Something went wrong.',
    ];

    /**
     * A list of possible warning messages.
     * @var array
     */
    private $warningMessage = [
        'Please log in before submitting a report.',
    ];

    /**
     * A list of possible success messages.
     * @var array
     */
    private $successMessage = [
        'Your report has been submitted.',
    ];

    /**
     * Creates a report from the given inputs.
     * @return mixed
     */
    public function report() {
        if (Auth::check()) {
            // Validate the report if the user is logged in
            $item_id = Input::get('item_id');
            $item_type = Input::get('item_type');
            $validation = Validator::make(
                Input::all(),
                ['report_reason' => 'required|max:500']
            );
            if ($validation->fails()) {
                Session::flash('errorMessage', $validation->errors()->first());
                return Redirect::back()->withInput();
            }
            $report_reason = Input::get('report_reason');
            try {
                // Create the report
                Report::create([
                    'user_id' => Auth::id(),
                    'item_id' => $item_id,
                    'item_type' => $item_type,
                    'report_reason' => $report_reason,
                ]);
            } catch (Exception $exc) {
                Log::error($exc);
                Session::flash('errorMessage', $this->errorMessage[0]);
                return Redirect::back()->withInput();
            }
        } else {
            // Redirect the user to log in otherwise
            Session::flash('warningMessage', $this->warningMessage[0]);
            return Redirect::route('login');
        }
        // Redirect the user back (success)
        Session::flash('successMessage', $this->successMessage[0]);
        return Redirect::back();
    }
}
