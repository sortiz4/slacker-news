<?php

class CommentController extends BaseController {
    /**
     * A list of possible error messages.
     * @var array
     */
    private $errorMessage = [
        'Something went wrong.',
    ];

    /**
     * A list of possible warning messages.
     * @var array
     */
    private $warningMessage = [
        'Please log in before commenting.',
        'Please log in before voting.',
    ];

    /**
     * A list of possible success messages.
     * @var array
     */
    private $successMessage = [
        'Your comment has been submitted.',
    ];

    /**
     * Creates a comment from the given inputs.
     * @return mixed
     */
    public function comment() {
        if (Auth::check()) {
            // Validate the comment if the user is logged in
            $parent_id = Input::get('parent_id');
            $parent_type = Input::get('parent_type');
            $validation = Validator::make(
                Input::all(),
                ['comment_content' => 'required|max:10000']
            );
            if ($validation->fails()) {
                Session::flash('errorMessage', $validation->errors()->first());
                return Redirect::back()->withInput();
            }
            $comment_content = Input::get('comment_content');
            try {
                // Create the comment
                Comment::create([
                    'user_id' => Auth::id(),
                    'parent_id' => $parent_id,
                    'parent_type' => $parent_type,
                    'comment_content' => $comment_content,
                    'vote_count' => 0,
                ]);
            } catch (Exception $exc) {
                Log::error($exc);
                Session::flash('errorMessage', $this->errorMessage[0]);
                return Redirect::back()->withInput();
            }
        } else {
            // Redirect the user to log in otherwise
            Session::flash('warningMessage', $this->warningMessage[0]);
            return Redirect::route('login');
        }
        // Redirect the user back (success)
        Session::flash('successMessage', $this->successMessage[0]);
        return Redirect::back();
    }

    /**
     * Allows a user to vote on a comment.
     * @param $comment_id
     * @return mixed
     */
    public function vote($comment_id) {
        if (Auth::check()) {
            // Fetch the first vote record
            $vote = (
                Vote::withTrashed()
                    ->where('user_id', '=', Auth::id())
                    ->where('parent_id', '=', $comment_id)
                    ->where('parent_type', '=', 'COMMENT')
                    ->first()
            );
            try {
                if (!empty($vote)) {
                    if (!empty($vote->deleted_at)) {
                        // Restore the soft deleted vote and increment the vote count
                        $vote->restore();
                        DB::transaction(function() use($comment_id) {
                            Comment::find($comment_id)->increment('vote_count');
                        });
                    } else {
                        // Soft delete the vote and decrement the vote count
                        $vote->delete();
                        DB::transaction(function() use($comment_id) {
                            Comment::find($comment_id)->decrement('vote_count');
                        });
                    }
                } else {
                    // Create a vote record and increment the vote count
                    Vote::create([
                        'user_id' => Auth::id(),
                        'parent_id' => $comment_id,
                        'parent_type' => 'COMMENT',
                    ]);
                    DB::transaction(function() use($comment_id) {
                        Comment::find($comment_id)->increment('vote_count');
                    });
                }
            } catch (Exception $exc) {
                Log::error($exc);
                Session::flash('errorMessage', $this->errorMessage[0]);
                return Response::json([
                    'success' => false,
                    'location' => URL::route('login'),
                ]);
            }
        } else {
            // Redirect the user to log in otherwise
            Session::flash('warningMessage', $this->warningMessage[1]);
            return Response::json([
                'success' => false,
                'location' => URL::route('login'),
            ]);
        }
        // Update the view with the new vote count
        return Response::json(['success' => true]);
    }
}
