@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: Log in</title>
@stop

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css" rel="stylesheet"/>
@stop

@section('content')
    <div class="container">
        {{
            Form::open([
                'route' => 'login',
                'method' => 'POST',
                'class' => 'well form-horizontal center-block form-width',
            ])
        }}
        <fieldset>
            <legend>Please log in.</legend>
            <div class="form-group form-padding">
                {{
                    Form::text(
                        'username',
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Username',
                            'maxlength' => '15',
                            'required',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::password(
                        'password',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Password',
                            'maxlength' => '60',
                            'required',
                        ]
                    )
                }}
                <div class="checkbox">
                    <label>
                        {{ Form::checkbox('remember', 'TRUE') }} Remember me
                    </label>
                </div>
            </div>
            <div class="form-group form-padding">
                {{
                    Form::submit(
                        'Log in',
                        ['class' => 'btn btn-md btn-primary btn-block']
                    )
                }}
            </div>
            <div class="text-center">
                Don't have an account?
                <a class="plain" href="{{ URL::route('signup') }}">
                    Sign up
                </a>
                .
            </div>
            <br/>
            <div class="text-center social">
                <a class="btn btn-lg btn-social-icon btn-facebook">
                    <span class="fa fa-facebook"></span>
                </a>
                <a class="btn btn-lg btn-social-icon btn-twitter">
                    <span class="fa fa-twitter"></span>
                </a>
                <a class="btn btn-lg btn-social-icon btn-google"
                   href="{{ URL::route('login.google') }}">
                    <span class="fa fa-google"></span>
                </a>
            </div>
        </fieldset>
        {{ Form::close() }}
    </div>
@stop

@section('scripts')
    <script src="https://use.fontawesome.com/c6cbdb5ea9.js"></script>
@stop
