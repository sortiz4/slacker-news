@extends('layouts.master')

@section('title')
    <title>
        {{ Config::get('app.title') }}:
        {{ HTML::entities($post->post_title) }}
    </title>
@stop

@section('content')
    <div class="container">
        <!-- Post table -->
        <table class="post" id="post">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            @include('partials.post', $post)
        </table>
        <!-- Post body -->
        @if ($post->post_type == 'TEXT' && !empty($post->post_content))
            <div class="item-content">
                {{ Markdown::string(html_disable($post->post_content)) }}
            </div>
        @endif
        <!-- Reply and report -->
        <div class="item-content">
            <!-- Toggle buttons -->
            <a class="label label-default toggle-reply-btn" href="javascript:void(0)">
                Reply
            </a>
            <a class="label label-default toggle-report-btn" href="javascript:void(0)">
                Report
            </a>
            <!-- Reply group -->
            <div class="hide-init toggle-reply">
                <br/>
                {{
                    Form::open([
                        'route' => 'comment',
                        'method' => 'POST',
                        'class' => 'well form-horizontal center-block',
                    ])
                }}
                <fieldset>
                    <legend>Reply to this post.</legend>
                    {{ Form::hidden('parent_type', 'POST') }}
                    {{ Form::hidden('parent_id', $post->post_id) }}
                    <div class="form-group form-padding">
                        {{
                            Form::textarea(
                                'comment_content',
                                null,
                                [
                                    'class' => 'form-control',
                                    'id' => 'textArea',
                                    'placeholder' => 'Comment',
                                    'maxlength' => '10000',
                                ]
                            )
                        }}
                        <span class="help-block">
                            We support
                            <a class="plain" href="{{ Config::get('urls.markdown_guide') }}">
                                Markdown
                            </a>
                            .
                        </span>
                    </div>
                    <div class="form-group form-padding">
                        {{
                            Form::submit(
                                'Submit',
                                ['class' => 'btn btn-md btn-primary']
                            )
                        }}
                        {{
                            Form::button(
                                'Cancel',
                                ['class' => 'btn btn-md btn-default toggle-reply-cnc']
                            )
                        }}
                    </div>
                </fieldset>
                {{ Form::close() }}
            </div>
            <!-- Report group -->
            <div class="hide-init toggle-report">
                <br/>
                {{
                    Form::open([
                        'route' => 'report',
                        'method' => 'POST',
                        'class' => 'well form-horizontal center-block',
                    ])
                }}
                <fieldset>
                    <legend>Report this post.</legend>
                    {{ Form::hidden('item_type', 'POST') }}
                    {{ Form::hidden('item_id', $post->post_id) }}
                    <div class="form-group form-padding">
                        {{
                            Form::textarea(
                                'report_reason',
                                null,
                                [
                                    'class' => 'form-control',
                                    'id' => 'textArea',
                                    'placeholder' => 'Reason',
                                    'maxlength' => '500',
                                ]
                            )
                        }}
                    </div>
                    <div class="form-group form-padding">
                        {{
                            Form::submit(
                                'Submit',
                                ['class' => 'btn btn-md btn-primary']
                            )
                        }}
                        {{
                            Form::button(
                                'Cancel',
                                ['class' => 'btn btn-md btn-default toggle-report-cnc']
                            )
                        }}
                    </div>
                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
        <br/>
        <!-- Comments label -->
        <div class="item-title">
            <h4>Comments</h4>
        </div>
        <hr/>
        <!-- All comments -->
        <div id="comment">
            @foreach ($comments as $comment)
                @include('partials.comment', $comment)
                @if ($comment->comments->count() > 0)
                    @include(
                        'recursives.comment',
                        $comments = (
                            Comment::where('parent_id', '=', $comment->comment_id)
                                ->where('parent_type', '=', 'COMMENT')
                                ->get()
                        )
                    )
                @endif
                <hr/>
            @endforeach
        </div>
        <!-- Load more comments -->
        <ul class="pager">
            <li>
                <a href="#">
                    Show more
                </a>
            </li>
        </ul>
    </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('js/toggle.js') }}"></script>
    @include('partials.vote.post')
    @include('partials.vote.comment')
@stop
