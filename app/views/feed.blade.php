@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}</title>
@stop

@section('content')
    <div class="container">
        <!-- Post table -->
        <table class="post" id="post">
            <colgroup>
                <col/>
                <col/>
                <col/>
            </colgroup>
            @foreach ($posts as $post)
                @include('partials.post', $post)
                <!-- Hr -->
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr>
            @endforeach
        </table>
        <!-- Next page -->
        <ul class="pager">
            {{ $posts->appends(['sort' => 'posts'])->links() }}
        </ul>
    </div>
@stop

@section('scripts')
    @include('partials.vote.post')
@stop
