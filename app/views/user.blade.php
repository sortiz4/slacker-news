@extends('layouts.master')

@section('title')
    <title>
        {{ Config::get('app.title') }}:
        @if (Route::currentRouteName() == 'user.posts')
            {{ $user->username }}'s Posts
        @elseif (Route::currentRouteName() == 'user.comments')
            {{ $user->username }}'s Comments
        @elseif (Route::currentRouteName() == 'user.favorites')
            {{ $user->username }}'s Favorites
        @else
            {{ $user->username }}'s Profile
        @endif
    </title>
@stop

@section('content')
    <div class="container">
        <h4>
            {{ $user->username }}, member since {{ date("F j, Y", strtotime($user->created_at)) }}
        </h4>
        <div class="well well-sm">
            <ul class="nav nav-pills">
                @if (Route::currentRouteName() == 'user')
                    <li class="active">
                @else
                    <li>
                @endif
                        <a href="{{ URL::route('user', $user->username) }}">
                            Profile
                        </a>
                    </li>
                @if (Route::currentRouteName() == 'user.posts')
                    <li class="active">
                @else
                    <li>
                @endif
                        <a href="{{ URL::route('user.posts', $user->username) }}">
                            Posts
                            <span class="badge">
                                {{ $user->posts->count() }}
                            </span>
                        </a>
                    </li>
                @if (Route::currentRouteName() == 'user.comments')
                    <li class="active">
                @else
                    <li>
                @endif
                        <a href="{{ URL::route('user.comments', $user->username) }}">
                            Comments
                            <span class="badge">
                                {{ $user->comments->count() }}
                            </span>
                        </a>
                    </li>
                @if (Route::currentRouteName() == 'user.favorites')
                    <li class="active">
                @else
                    <li>
                @endif
                        <a href="{{ URL::route('user.favorites', $user->username) }}">
                            Favorites
                            <span class="badge">
                                {{ Vote::where('user_id', '=', $user->user_id)->count() }}
                            </span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            Karma
                            <span class="badge">
                                {{ karma($user->user_id) }}
                            </span>
                        </a>
                    </li>
            </ul>
        </div>
        @if (Route::currentRouteName() == 'user.posts')
            <br/>
            <h4>Posts</h4>
            <hr/>
            <!-- Post table -->
            <table class="post" id="post">
                <colgroup>
                    <col/>
                    <col/>
                    <col/>
                </colgroup>
            @foreach ($posts as $post)
                @include('partials.post', $post)
                <!-- Hr -->
                <tr>
                    <td colspan="3">
                        <hr/>
                    </td>
                </tr>
            @endforeach
            </table>
            <!-- Load more -->
            <ul class="pager">
                <li>
                    <a href="#">
                        Show more
                    </a>
                </li>
            </ul>
        @elseif (Route::currentRouteName() == 'user.comments')
            <br/>
            <h4>Comments</h4>
            <hr/>
            <div id="comment">
                @foreach ($comments as $comment)
                    @include('partials.comment', $comment)
                    <hr/>
                @endforeach
            </div>
            <!-- Load more -->
            <ul class="pager">
                <li>
                    <a href="#">
                        Show more
                    </a>
                </li>
            </ul>
        @elseif (Route::currentRouteName() == 'user.favorites')
            <br/>
            <h4>Favorites</h4>
            <hr/>
            <div id="post">
                <div id="comment">
                    @foreach ($votes as $vote)
                        @if ($vote->parent_type == 'POST')
                            <table class="post">
                                <colgroup>
                                    <col/>
                                    <col/>
                                    <col/>
                                </colgroup>
                                @include('partials.post', $post = Post::find($vote->parent_id))
                            </table>
                        @else
                            @include('partials.comment', $comment = Comment::find($vote->parent_id))
                        @endif
                        <hr/>
                    @endforeach
                </div>
            </div>
            <!-- Load more -->
            <ul class="pager">
                <li>
                    <a href="#">
                        Show more
                    </a>
                </li>
            </ul>
        @else
            <!-- User profile -->
        @endif
    </div>
@stop

@section('scripts')
    <script src="{{ URL::asset('js/toggle.js') }}"></script>
    @include('partials.vote.post')
    @include('partials.vote.comment')
@stop
