<!-- Post head -->
<tr data-post-id="{{ $post->post_id }}">
    <td class="item-btn">
        <div class="title">
            <h4>
                <a class="glyphicon glyphicon-chevron-up plain"
                   data-role="vote-post" href="javascript:void(0)">
                </a>
            </h4>
        </div>
    </td>
    <td class="item-vote">
        <div class="title">
            <h4 class="text-center" data-id="vote_count">
                {{ $post->vote_count }}
            </h4>
        </div>
    </td>
    <td class="item-title">
        <div class="title">
            <h4>
                @if ($post->post_type == 'LINK')
                    <a class="plain" href="{{ $post->post_content }}">
                @elseif ($post->post_type == 'TEXT')
                    <a class="plain" href="{{ URL::route('post', $post->post_id) }}">
                @endif
                        {{ HTML::entities($post->post_title) }}
                    </a>
                @if ($post->post_type == 'LINK' && $host = get_host($post->post_content))
                    <a class="h6 plain text-muted" href="{{ $host['link'] }}">
                        ({{ $host['host'] }})
                    </a>
                @endif
            </h4>
            <h5>
                submitted {{ $post->created_at->diffForHumans() }} by
                <a class="plain" href="{{ URL::route('user', $post->user->username) }}">
                    {{ $post->user->username }}
                </a>
            </h5>
            <h6>
                <b>
                    <a class="plain" href="{{ URL::route('post', $post->post_id) }}">
                        {{ $post->countComments() }} comments
                    </a>
                </b>
            </h6>
        </div>
    </td>
</tr>
