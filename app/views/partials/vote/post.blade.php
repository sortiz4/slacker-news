<script type="text/javascript">
    $(function () {
        var $this = $('#post');
        $this.on('click', '[data-role="vote-post"]', function () {
            var tag = 'data-post-id';
            var id = $(this).closest('[' + tag + ']').attr(tag);
            $.ajax({
                method: 'POST',
                url: "{{ URL::route('feed') }}/vote/post/" + id,
                success: function (data) {
                    if (data) {
                        console.log(data.message);
                        if (data.success) {
                            var find = '[' + tag + '="' + id + '"] [data-id="vote_count"]';
                            $this.find(find).text(data.vote_count);
                        } else {
                            window.location = data.location;
                        }
                    }
                },
            });
        });
    });
</script>
