<script type="text/javascript">
    $(function () {
        $('#comment').on('click', '[data-role="vote-comment"]', function () {
            var tag = 'data-comment-id';
            var id = $(this).closest('[' + tag + ']').attr(tag);
            $.ajax({
                method: 'POST',
                url: "{{ URL::route('feed') }}/vote/comment/" + id,
                success: function (data) {
                    if (data) {
                        console.log(data.message);
                        if (data.success === false) {
                            window.location = data.location;
                        }
                    }
                },
            });
        });
    });
</script>
