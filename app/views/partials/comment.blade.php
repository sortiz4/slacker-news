<!-- Comment head -->
<table>
    <tr data-comment-id="{{ $comment->comment_id }}">
        <td class="item-btn">
            <div class="title">
                <h5>
                    <a class="glyphicon glyphicon-chevron-up plain"
                       data-role="vote-comment" href="javascript:void(0)">
                    </a>
                </h5>
            </div>
        </td>
        <td class="item-title">
            <div class="title">
                <h5>
                    <a class="plain" href="{{ URL::route('user', $comment->user->username) }}">
                        {{ $comment->user->username }}
                    </a>
                    , {{ $comment->created_at->diffForHumans() }}
                </h5>
            </div>
        </td>
    </tr>
</table>

<!-- Comment body -->
<div class="item-content">
    {{ Markdown::string(html_disable($comment->comment_content)) }}
</div>

<!-- Reply and report -->
<div class="item-content">
    <!-- Toggle buttons -->
    <a class="label label-default toggle-reply-btn" href="javascript:void(0)">
        Reply
    </a>
    <a class="label label-default toggle-report-btn" href="javascript:void(0)">
        Report
    </a>
    <!-- Reply group -->
    <div class="hide-init toggle-reply">
        <br/>
        {{
            Form::open([
                'route' => 'comment',
                'method' => 'POST',
                'class' => 'well form-horizontal center-block',
            ])
        }}
        <fieldset>
            <legend>Reply to this comment.</legend>
            {{ Form::hidden('parent_type', 'COMMENT') }}
            {{ Form::hidden('parent_id', $comment->comment_id) }}
            <div class="form-group form-padding">
                {{
                    Form::textarea(
                        'comment_content',
                        null,
                        [
                            'class' => 'form-control',
                            'id' => 'textArea',
                            'placeholder' => 'Comment',
                            'maxlength' => '10000',
                        ]
                    )
                }}
                <span class="help-block">
                    We support
                    <a class="plain" href="{{ Config::get('urls.markdown_guide') }}">
                        Markdown
                    </a>
                    .
                </span>
            </div>
            <div class="form-group form-padding">
                {{
                    Form::submit(
                        'Submit',
                        ['class' => 'btn btn-md btn-primary']
                    )
                }}
                {{
                    Form::button(
                        'Cancel',
                        ['class' => 'btn btn-md btn-default toggle-reply-cnc']
                    )
                }}
            </div>
        </fieldset>
        {{ Form::close() }}
    </div>
    <!-- Report group -->
    <div class="hide-init toggle-report">
        <br/>
        {{
            Form::open([
                'route' => 'report',
                'method' => 'POST',
                'class' => 'well form-horizontal center-block',
            ])
        }}
        <fieldset>
            <legend>Report this comment.</legend>
            {{ Form::hidden('item_type', 'COMMENT') }}
            {{ Form::hidden('item_id', $comment->comment_id) }}
            <div class="form-group form-padding">
                {{
                    Form::textarea(
                        'report_reason',
                        null,
                        [
                            'class' => 'form-control',
                            'id' => 'textArea',
                            'placeholder' => 'Reason',
                            'maxlength' => '500',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::submit(
                        'Submit',
                        ['class' => 'btn btn-md btn-primary']
                    )
                }}
                {{
                    Form::button(
                        'Cancel',
                        ['class' => 'btn btn-md btn-default toggle-report-cnc']
                    )
                }}
            </div>
        </fieldset>
        {{ Form::close() }}
    </div>
</div>
