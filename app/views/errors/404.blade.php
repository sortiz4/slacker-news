@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: Page not found.</title>
@stop

@section('content')
    <!-- Container -->
    <div class="container">
        <div class="well">
            <legend>Page not found.</legend>
            <p>The page you requested does not exist.</p>
        </div>
    </div>
@stop
