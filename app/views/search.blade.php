@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: Search</title>
@stop

@section('content')
    <div class="container">
        {{
            Form::open([
                'route' => 'search',
                'method' => 'GET',
                'class' => 'well form-horizontal center-block',
            ])
        }}
        <fieldset>
            <legend>Search for something.</legend>
            <div class="input-group">
                {{
                    Form::text(
                        'q',
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Search',
                            'maxlength' => '255',
                            'required',
                        ]
                    )
                }}
                <span class="input-group-btn">
                    {{
                        Form::submit(
                            'Submit',
                            ['class' => 'btn btn-default']
                        )
                    }}
                </span>
            </div>
        </fieldset>
        {{ Form::close() }}
        @if (Input::has('q'))
            <br/>
            <h4>Posts</h4>
            <hr/>
            @if ($posts->isEmpty())
                <p>There doesn't seem to be anything here.</p>
                <br/>
            @else
                <!-- Post table -->
                <table class="post" id="post">
                    <colgroup>
                        <col/>
                        <col/>
                        <col/>
                    </colgroup>
                    @foreach ($posts as $post)
                        @include('partials.post', $post)
                        <!-- Hr -->
                        <tr>
                            <td colspan="3">
                                <hr/>
                            </td>
                        </tr>
                    @endforeach
                </table>
            @endif
            <h4>Comments</h4>
            <hr/>
            @if ($comments->isEmpty())
                <p>There doesn't seem to be anything here.</p>
                <br/>
            @else
                <div id="comment">
                    @foreach ($comments as $comment)
                        @include('partials.comment', $comment)
                        <hr/>
                    @endforeach
                </div>
            @endif
        @endif
    </div>
@stop

@section('scripts')
    @if (Input::has('q'))
        <script src="{{ URL::asset('js/toggle.js') }}"></script>
        @include('partials.vote.post')
        @include('partials.vote.comment')
    @endif
@stop
