<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @yield('meta')
        @yield('title')
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/lumen/bootstrap.min.css" rel="stylesheet"/>
        <link href="{{ URL::asset('css/main.css') }}" rel="stylesheet"/>
        @yield('styles')
    </head>
    <body>
        <!-- Navigation bar -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle button -->
                <div class="navbar-header">
                    <button class="navbar-toggle collapsed" data-target="#navbar-collapse"
                            data-toggle="collapse" type="button">
                        <span class="sr-only">
                            Toggle navigation
                        </span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ URL::route('feed') }}">
                        {{ Config::get('app.title') }}
                    </a>
                </div>
                <!-- Collapsible elements -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <!-- Submit menu -->
                    <ul class="nav navbar-nav navbar-top">
                        @if (Route::currentRouteName() == 'submit.link' || Route::currentRouteName() == 'submit.text')
                            <li class="dropdown active">
                                <a class="dropdown-toggle" data-toggle="dropdown">
                                    Submit
                                    <span class="caret">
                                        <span class="sr-only">
                                            (current)
                                        </span>
                                    </span>
                                </a>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Submit
                                    <span class="caret"></span>
                                </a>
                        @endif
                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ URL::route('submit.link') }}">
                                            Link
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ URL::route('submit.text') }}">
                                            Text
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        @if (Route::currentRouteName() == 'search')
                            <li class="active">
                                <a href="{{ URL::route('search') }}">
                                    Search
                                    <span class="sr-only">
                                        (current)
                                    </span>
                                </a>
                            </li>
                        @else
                            <li>
                                <a href="{{ URL::route('search') }}">
                                    Search
                                </a>
                            </li>
                        @endif
                    </ul>
                    <!-- Sign up and log in or username and logout -->
                    <ul class="nav navbar-nav navbar-right navbar-bottom">
                        @if (Auth::check())
                            @if (isset($username) && Auth::user()->username == $username)
                                <li class="active">
                                    <a href="{{ URL::route('user', [Auth::user()->username]) }}">
                                        {{ Auth::user()->username }}
                                        <span class="sr-only">
                                            (current)
                                        </span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ URL::route('user', [Auth::user()->username]) }}">
                                        {{ Auth::user()->username }}
                                    </a>
                                </li>
                            @endif
                            <li>
                                <a href="{{ URL::route('logout') }}">
                                    Log out
                                </a>
                            </li>
                        @else
                            @if (Route::currentRouteName() == 'signup')
                                <li class="active">
                                    <a href="{{ URL::route('signup') }}">
                                        Sign up
                                        <span class="sr-only">
                                            (current)
                                        </span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ URL::route('signup') }}">
                                        Sign up
                                    </a>
                                </li>
                            @endif
                            @if (Route::currentRouteName() == 'login')
                                <li class="active">
                                    <a href="{{ URL::route('login') }}">
                                        Log in
                                        <span class="sr-only">
                                            (current)
                                        </span>
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ URL::route('login') }}">
                                        Log in
                                    </a>
                                </li>
                            @endif
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        @if (Session::has('errorMessage'))
            <!-- Error message -->
            @if (Route::currentRouteName() == 'login' || Route::currentRouteName() == 'signup')
                <div class="container">
                    <div class="alert alert-dismissible alert-danger center-block form-width">
            @else
                <div class="container alert-width">
                    <div class="alert alert-dismissible alert-danger center-block">
            @endif
                        <button type="button" class="close" data-dismiss="alert">
                            &times;
                        </button>
                        <strong>
                            Error!
                        </strong>
                        {{ Session::get('errorMessage') }}
                    </div>
                </div>
        @endif
        @if (Session::has('warningMessage'))
            <!-- Warning message -->
            @if (Route::currentRouteName() == 'login' || Route::currentRouteName() == 'signup')
                <div class="container">
                    <div class="alert alert-dismissible alert-warning center-block form-width">
            @else
                <div class="container alert-width">
                    <div class="alert alert-dismissible alert-warning center-block">
            @endif
                        <button type="button" class="close" data-dismiss="alert">
                            &times;
                        </button>
                        <strong>
                            Warning!
                        </strong>
                        {{ Session::get('warningMessage') }}
                    </div>
                </div>
        @endif
        @if (Session::has('successMessage'))
            <!-- Success message -->
            @if (Route::currentRouteName() == 'login'
                || Route::currentRouteName() == 'signup')
                <div class="container">
                    <div class="alert alert-dismissible alert-success center-block form-width">
            @else
                <div class="container alert-width">
                    <div class="alert alert-dismissible alert-success center-block">
            @endif
                        <button type="button" class="close" data-dismiss="alert">
                            &times;
                        </button>
                        <strong>
                            Success!
                        </strong>
                        {{ Session::get('successMessage') }}
                    </div>
                </div>
        @endif
        <!-- Content -->
        @yield('content')
        <!-- Footer -->
        <div class="container">
            <footer>
                <p class="pull-left">
                    <a href="{{ URL::route('about') }}">
                        About
                    </a>
                </p>
                <p class="pull-right">
                    <a href="#">
                        Back to top
                    </a>
                </p>
            </footer>
        </div>
    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    @yield('scripts')
</html>
