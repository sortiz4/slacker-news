@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: Submit</title>
@stop

@section('content')
    <div class="container">
        {{
            Form::open([
                'route' => 'submit',
                'method' => 'POST',
                'class' => 'well form-horizontal center-block',
            ])
        }}
        <fieldset>
            @if ($type == 'LINK')
                <legend>Submit a link.</legend>
                {{ Form::hidden('type', 'LINK') }}
                <div class="form-group form-padding">
                    {{
                        Form::text(
                            'post_title',
                            null,
                            [
                                'class' => 'form-control',
                                'placeholder' => 'Title',
                                'maxlength' => '255',
                                'required',
                            ]
                        )
                    }}
                </div>
                <div class="form-group form-padding">
                    {{
                        Form::text(
                            'post_content',
                            null,
                            [
                                'class' => 'form-control',
                                'placeholder' => 'URL',
                                'maxlength' => '2000',
                                'required',
                            ]
                        )
                    }}
                </div>
            @else
                <legend>Submit a text post.</legend>
                {{ Form::hidden('type', 'TEXT') }}
                <div class="form-group form-padding">
                    {{
                        Form::text(
                            'post_title',
                            null,
                            [
                                'class' => 'form-control',
                                'placeholder' => 'Title',
                                'maxlength' => '255',
                                'required',
                            ]
                        )
                    }}
                </div>
                <div class="form-group form-padding">
                    {{
                        Form::textarea(
                            'post_content',
                            null,
                            [
                                'class' => 'form-control',
                                'id' => 'textArea',
                                'placeholder' => 'Post (optional)',
                                'maxlength' => '10000',
                            ]
                        )
                    }}
                    <span class="help-block">
                        We support
                        <a class="plain" href="{{ Config::get('urls.markdown_guide') }}">
                           Markdown
                        </a>
                        .
                    </span>
                </div>
            @endif
            <div class="form-group form-padding">
                {{ Form::submit('Submit', ['class' => 'btn btn-md btn-primary']) }}
            </div>
        </fieldset>
        {{ Form::close() }}
    </div>
@stop
