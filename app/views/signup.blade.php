@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: Sign up</title>
@stop

@section('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css" rel="stylesheet"/>
@stop

@section('content')
    <div class="container">
        {{
            Form::open([
                'route' => 'signup',
                'method' => 'POST',
                'class' => 'well form-horizontal center-block form-width',
            ])
        }}
        <fieldset>
            <legend>Join today.</legend>
            <div class="form-group form-padding">
                {{
                    Form::text(
                        'username',
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Username',
                            'maxlength' => '15',
                            'required',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::email(
                        'email',
                        null,
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Email',
                            'maxlength' => '255',
                            'required',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::password(
                        'password',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Password',
                            'maxlength' => '60',
                            'required',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::password(
                        'password_confirmation',
                        [
                            'class' => 'form-control',
                            'placeholder' => 'Confirm password',
                            'maxlength' => '60',
                            'required',
                        ]
                    )
                }}
            </div>
            <div class="form-group form-padding">
                {{
                    Form::submit(
                        'Sign up',
                        ['class' => 'btn btn-md btn-primary btn-block']
                    )
                }}
            </div>
            <div class="text-center">
                Already have an account?
                <a class="plain" href="{{ URL::route('login') }}">
                    Log in
                </a>
                .
            </div>
            <br/>
            <div class="text-center social">
                <a class="btn btn-lg btn-social-icon btn-facebook">
                    <span class="fa fa-facebook"></span>
                </a>
                <a class="btn btn-lg btn-social-icon btn-twitter">
                    <span class="fa fa-twitter"></span>
                </a>
                <a class="btn btn-lg btn-social-icon btn-google"
                   href="{{ URL::route('signup.google') }}">
                    <span class="fa fa-google"></span>
                </a>
            </div>
        </fieldset>
        {{ Form::close() }}
    </div>
@stop

@section('scripts')
    <script src="https://use.fontawesome.com/c6cbdb5ea9.js"></script>
@stop
