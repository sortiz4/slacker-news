@extends('layouts.master')

@section('title')
    <title>{{ Config::get('app.title') }}: About</title>
@stop

@section('content')
    <div class="container">
        <div class="well">
            <legend>About {{ Config::get('app.title') }}</legend>
            <p>
                Created by Matt Abrego, Brandon Barrett, Sanjay Nadaraja, and Steven Ortiz.
                <br/><br/>
                UTSA, Summer 2016, CS 4413-01T Web Technologies (team project).
            </p>
        </div>
    </div>
@stop
