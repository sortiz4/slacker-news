@foreach ($comments as $comment)
    <blockquote>
        @include('partials.comment', $comment)
        @if ($comment->comments->count() > 0)
            @include(
                'recursives.comment',
                $comments = (
                    Comment::where('parent_id', '=', $comment->comment_id)
                        ->where('parent_type', '=', 'COMMENT')
                        ->get()
                )
            )
        @endif
    </blockquote>
@endforeach
