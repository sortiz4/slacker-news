<?php

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     * @return void
     */
    public function run() {
        touch(Config::get('database.connections.sqlite.database'));
        Eloquent::unguard();
    }
}
