<?php

use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('votes', function($table) {
            // Primary key
            $table->bigIncrements('vote_id');

            // User ID of the creator
            $table->integer('user_id')->unsigned();

            // User ID is a foreign key
            $table->foreign('user_id')->references('user_id')->on('users');

            // Parent ID of the object
            $table->integer('parent_id')->unsigned();

            // Users may vote on posts and comments
            $table->enum('parent_type', ['COMMENT', 'POST']);

            // Soft deletes a record if the user changes their vote
            $table->softDeletes();

            // Created and updated timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('votes');
    }
}
