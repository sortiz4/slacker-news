<?php

use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('posts', function($table) {
            // Primary key
            $table->increments('post_id');

            // User ID of the creator
            $table->integer('user_id')->unsigned();

            // User ID is a foreign key
            $table->foreign('user_id')->references('user_id')->on('users');

            // Posts may be links or text
            $table->enum('post_type', ['LINK', 'TEXT']);

            // Limit to 255 characters
            $table->string('post_title', 255);

            // Limit to 10,000 characters
            $table->text('post_content');

            // The number votes
            $table->integer('vote_count')->unsigned();

            // If edited, a timestamp will exist
            $table->timestamp('edited_at')->nullable();

            // Created and updated timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('posts');
    }
}
