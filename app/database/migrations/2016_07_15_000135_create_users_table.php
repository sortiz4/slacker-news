<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('users', function($table) {
            // Primary key
            $table->increments('user_id');

            // Limit to 15 characters [0-9A-Za-z_-]+
            $table->string('username', 15)->unique();

            // Limit to 60 characters (minimum of 5)
            $table->string('password', 60);

            // Limit to 255 characters
            $table->string('email', 255)->unique();

            // Authentication token
            $table->rememberToken();

            // Created and updated timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('users');
    }
}
