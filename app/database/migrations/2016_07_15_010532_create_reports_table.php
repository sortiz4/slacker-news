<?php

use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('reports', function($table) {
            // Primary key
            $table->increments('report_id');

            // User ID of the creator
            $table->integer('user_id')->unsigned();

            // User ID is a foreign key
            $table->foreign('user_id')->references('user_id')->on('users');

            // Item ID of the object
            $table->integer('item_id')->unsigned();

            // Users may report posts, comments, and other users
            $table->enum('item_type', ['COMMENT', 'POST', 'USER']);

            // Limit to 500 characters
            $table->text('report_reason');

            // Created and updated timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('reports');
    }
}
