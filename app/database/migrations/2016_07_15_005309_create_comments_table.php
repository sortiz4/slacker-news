<?php

use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {
    /**
     * Run the migrations.
     * @return void
     */
    public function up() {
        Schema::create('comments', function($table) {
            // Primary key
            $table->increments('comment_id');

            // User ID of the creator
            $table->integer('user_id')->unsigned();

            // User ID is a foreign key
            $table->foreign('user_id')->references('user_id')->on('users');

            // Parent ID of the object
            $table->integer('parent_id')->unsigned();

            // Comments may belong to posts and other comments
            $table->enum('parent_type', ['COMMENT', 'POST']);

            // Limit to 10,000 characters
            $table->text('comment_content');

            // The number votes
            $table->integer('vote_count')->unsigned();

            // If edited, a timestamp will exist
            $table->timestamp('edited_at')->nullable();

            // Created and updated timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down() {
        Schema::drop('comments');
    }
}
