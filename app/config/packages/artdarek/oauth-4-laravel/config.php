<?php

return [
    // OAuth storage
    'storage' => 'Session',

    // OAuth consumers
    'consumers' => [
        'Facebook' => [
            'client_id' => '',
            'client_secret' => '',
            'scope' => [],
        ],
        'Twitter' => [
            'client_id' => '',
            'client_secret' => '',
            'scope' => [],
        ],
        'Google' => [
            'client_id' => $_ENV['GOOGLE_ID'],
            'client_secret' => $_ENV['GOOGLE_SECRET'],
            'scope' => ['userinfo_email', 'userinfo_profile'],
        ],
    ]
];
