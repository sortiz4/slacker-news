<?php

return [
    // Markdown storage paths
    'paths' => ['/markdown'],

    // Markdown routes paths
    'routes' => [],

    // Markdown routes extensions
    'extensions' => ['markdown', 'md'],

    // Add routes for the file extensions
    'add_routes' => true,

    // PHP markdown options
    'options' => [
        'use_extra' => true,
        'empty_element_suffix' => '>',
        'tab_width' => 4,
        'no_markup' => false,
        'no_entities' => false,
        'predef_urls' => [],
        'predef_titles' => [],
        'fn_id_prefix' => '',
        'fn_link_title' => '',
        'fn_backlink_title' => '',
        'fn_link_class' => 'footnote-ref',
        'fn_backlink_class' => 'footnote-backref',
        'code_class_prefix' => '',
        'code_attr_on_pre' => false,
        'predef_abbr' => [],
    ],
];
