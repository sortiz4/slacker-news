<?php

/**
 * Disables HTML tags by substituting the opening brace with an HTML entity.
 * This will prevent block quotes from being disabled with Markdown.
 * @param $string
 * @return mixed
 */
function html_disable($string) {
    return preg_replace('/</', '&lt;', $string);
}

/**
 * This function returns the host of the given URL without the leading 'www'.
 * @param $string
 * @return mixed
 */
function get_host($string) {
    if ($url = parse_url($string)) {
        if (count(explode('.', $url['host'])) > 2) {
            $host = preg_replace('/^www\./', '', $url['host']);
        } else {
            $host = $url['host'];
        }
        $link = $url['scheme'].'://'.$host;
        return [
            'link' => $link,
            'host' => $host,
        ];
    }
    return false;
}

/**
 * This function returns the sum of a user's post and comment vote counts.
 * @param $user_id
 * @return int
 */
function karma($user_id) {
    return (
        Post::where('user_id', '=', $user_id)->sum('vote_count') +
        Comment::where('user_id', '=', $user_id)->sum('vote_count')
    );
}
