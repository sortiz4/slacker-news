<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
*/

// Global patterns
Route::pattern('id', '[0-9]+');
Route::pattern('username', '[0-9A-Za-z_-]+');

// Home routes
Route::get(
    '/',
    [
        'as' => 'feed',
        'uses' => 'HomeController@showHome',
    ]
);
Route::get(
    '/about',
    [
        'as' => 'about',
        'uses' => 'HomeController@showAbout',
    ]
);

// User routes
Route::group(['prefix' => 'user'], function() {
    Route::get(
        '/{username}',
        [
            'as' => 'user',
            'uses' => 'UserController@showUser',
        ]
    );
    Route::get(
        '/{username}/posts',
        [
            'as' => 'user.posts',
            'uses' => 'UserController@showUserPosts',
        ]
    );
    Route::get(
        '/{username}/comments',
        [
            'as' => 'user.comments',
            'uses' => 'UserController@showUserComments',
        ]
    );
    Route::get(
        '/{username}/favorites',
        [
            'as' => 'user.favorites',
            'uses' => 'UserController@showUserFavorites',
        ]
    );
});

// Log in and out routes
Route::get(
    '/login',
    [
        'as' => 'login',
        'uses' => 'AuthenticationController@showAuthentication',
    ]
);
Route::get(
    '/logout',
    [
        'as' => 'logout',
        'uses' => 'AuthenticationController@deauthentication',
    ]
);
Route::post(
    '/login',
    [
        'as' => 'login',
        'uses' => 'AuthenticationController@authentication',
    ]
);

// Sign up routes
Route::get(
    '/signup',
    [
        'as' => 'signup',
        'uses' => 'RegistrationController@showRegistration',
    ]
);
Route::post(
    '/signup',
    [
        'as' => 'signup',
        'uses' => 'RegistrationController@registration',
    ]
);

// Google routes
Route::match(
    ['GET', 'POST'],
    '/login/google',
    [
        'as' => 'login.google',
        'uses' => 'AuthenticationController@googleAuthentication',
    ]
);
Route::match(
    ['GET', 'POST'],
    '/signup/google',
    [
        'as' => 'signup.google',
        'uses' => 'RegistrationController@googleRegistration',
    ]
);

// Submission routes
Route::group(['prefix' => 'submit'], function() {
    Route::get(
        '/link',
        [
            'as' => 'submit.link',
            'uses' => 'PostController@showSubmitLink',
        ]
    );
    Route::get(
        '/text',
        [
            'as' => 'submit.text',
            'uses' => 'PostController@showSubmitText',
        ]
    );
    Route::get(
        '/',
        [
            'as' => 'submit',
            'uses' => 'ErrorController@missing',
        ]
    );
    Route::post(
        '/',
        [
            'as' => 'submit',
            'uses' => 'PostController@post',
        ]
    );
});

// Comment routes
Route::get(
    '/comment',
    [
        'as' => 'comment',
        'uses' => 'ErrorController@missing',
    ]
);
Route::post(
    '/comment',
    [
        'as' => 'comment',
        'uses' => 'CommentController@comment',
    ]
);

// Report routes
Route::get(
    '/report',
    [
        'as' => 'report',
        'uses' => 'ErrorController@missing',
    ]
);
Route::post(
    '/report',
    [
        'as' => 'report',
        'uses' => 'ReportController@report',
    ]
);

// Post routes
Route::get(
    '/post/{id}',
    [
        'as' => 'post',
        'uses' => 'PostController@showPost',
    ]
);

// Vote routes
Route::group(['prefix' => 'vote'], function() {
    Route::get(
        '/post/{id}',
        [
            'as' => 'vote.post',
            'uses' => 'ErrorController@missing',
        ]
    );
    Route::get(
        '/comment/{id}',
        [
            'as' => 'vote.comment',
            'uses' => 'ErrorController@missing',
        ]
    );
    Route::post(
        '/post/{id}',
        [
            'as' => 'vote.post',
            'uses' => 'PostController@vote',
        ]
    );
    Route::post(
        '/comment/{id}',
        [
            'as' => 'vote.comment',
            'uses' => 'CommentController@vote',
        ]
    );
});

// Search routes
Route::get(
    '/search',
    [
        'as' => 'search',
        'uses' => 'SearchController@showSearch',
    ]
);
