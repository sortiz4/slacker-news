jQuery(document).ready(function ($) {
    // Animation speed
    var speed = 350;

    // Reply toggles
    $('.toggle-reply-btn').click(function () {
        $(this).parent().find('.toggle-reply').slideToggle(speed);
    });
    $('.toggle-reply-cnc').click(function () {
        $(this).parent().closest('.toggle-reply').slideToggle(speed);
    });

    // Report toggles
    $('.toggle-report-btn').click(function () {
        $(this).parent().find('.toggle-report').slideToggle(speed);
    });
    $('.toggle-report-cnc').click(function () {
        $(this).parent().closest('.toggle-report').slideToggle(speed);
    });
});
