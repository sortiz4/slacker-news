# Slacker News
I made this web application for a web development class while I was an
undergraduate at UTSA back in 2016. It was my first web-based project, so
please keep that in mind if you decide to take a look at the code. I've
reformatted the project and made a few small changes.

## Requirements
- PHP 5.6 or 7.0
  - MCrypt
  - PDO
  - SQLite
- Composer 1.1

## Setup
Add `.env.php` to the to the project root with these settings.

```php
<?php

return [
    'APP_URL' => 'http://localhost',
    'APP_DEBUG' => true,
    'DB_CONNECTION' => 'sqlite',
    'GOOGLE_ID' => '',
    'GOOGLE_SECRET' => '',
    'MAILGUN_DOMAIN' => '',
    'MAILGUN_SECRET' => '',
];
```

Initialize the project and migrate the database with these commands.

```sh
$ composer install
$ php artisan db:seed
$ php artisan migrate
```

Run the application by executing `php artisan serve`.
